const PuppeteerEnvironment = require('jest-environment-puppeteer')

class CustomEnvironment extends PuppeteerEnvironment {
  async setup() {
    await super.setup()
    const server = this.global.puppeteerConfig || {}
    const host = server.host || 'localhost'
    const port = server.port || 9000
    const protocol = server.protocol || 'http'
    const originalGoto = this.global.page.goto

    this.global.page.goto = async function goto(pathOrURL) {
      const url = /^\//.test(pathOrURL)
        ? `${protocol}://${host}:${port}${pathOrURL}`
        : pathOrURL
      return originalGoto.call(this, url)
    }
  }
}

module.exports = CustomEnvironment


