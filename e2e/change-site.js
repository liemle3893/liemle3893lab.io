const puppeteer = require('puppeteer');

const timeout = 30000;

describe(
    '/ (Home Page)',
    () => {
        beforeAll(async () => {
            await page.goto('http://localhost:9000/');
        });
        it('should change to google.com when change click on cover image', async () => {
            // This link is the parent of cover image.
            page.click('a#google-link')
            await page.waitForNavigation()
            let currentUrl = page.url()
            expect(currentUrl).toBe('https://www.google.com/');
        }, timeout);
    },
    timeout
);