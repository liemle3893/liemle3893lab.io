const puppeteer = require('puppeteer');

const timeout = 30000;

describe(
  '/ (Home Page)',
  () => {
    beforeAll(async () => {
      await page.goto('http://localhost:9000/');
    }, timeout);

    it('should load without error', async () => {
      let title = await page.evaluate(() => document.title);;
      expect(title).toBe('Home | Getting started with CI/CD');
    });
  },
  timeout,
);