import React from "react"
import { useStaticQuery, graphql, StaticQuery, Link } from "gatsby"
import Img from "gatsby-image"

const PureImage = ({ data }) => {
  return <Link id="google-link" to={`https://google.com`} > <Img alt={`Devops lifecycle`} fluid={data.placeholderImage.childImageSharp.fluid} /> </Link>
}

const Image = props => {
  return <StaticQuery
    query={graphql`
  query {
    placeholderImage: file(relativePath: { eq: "devops.png" }) {
      childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
  }
`}
    render={data => <PureImage {...props} data={data} />}
  />
}

export default Image
