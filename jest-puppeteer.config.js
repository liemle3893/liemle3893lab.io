const CI = process.env.CI || false
const slowMotionMs = CI ? 0 : 250
const browser = process.env.BROWSER || "chromium"

module.exports = {
  launch: {
    headless: CI,
    slowMo: slowMotionMs,
    args: [
      'no-sandbox',
      'disable-setuid-sandbox',
    ]    
  },
  browser: browser,
  server: {
    command: './node_modules/.bin/gatsby serve',
    launchTimeout: 30000, // set to 30s
    port: 9000,
  },
};